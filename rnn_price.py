import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import yfinance as yf

btcusd_df = yf.download('BTC-USD', start='2020-01-01', end='2021-12-31')
btcusd_df.to_csv('BTC-USD.csv')

data = pd.read_csv('BTC-USD.csv', date_parser = True)
data.tail()

data_training = data[data['Date'] > '2020-01-01'].copy()

training_data = data_training.drop(['Date', 'Adj Close'], axis=1)

scaler = MinMaxScaler()
training_data = scaler.fit_transform(training_data)

X_train = []
Y_train = []

training_data.shape[0]
for i in range (60, training_data.shape[0]):
    X_train.append(training_data[i - 60 : i])
    Y_train.append(training_data[i, 0])

X_train, Y_train = np.array(X_train), np.array(Y_train)
print(X_train)
